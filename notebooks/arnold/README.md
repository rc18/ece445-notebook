# Project Updates

## 2/1/2024
Project approved and group finalized! \
Objective was to get an idea that was approved, and find a team. \
Submitted the idea, then it was approved. So Objective was accomplished.


## 2/6/2024
Objective was to meet with our TA, and get started on project proposal. \
First TA meeting with Selva from 6-6:30pm and got our locker. \
Got the guidelines for project proposal, and worked on project proposal.


## 2/8/2024 and 2/9/2024
Objective is to figure out what sensors to use for our project and finish project proposal and team contract. \
Started looking around for sensors, narrowed it down to pH and temperature. Looking for Chlorine sensors, but it is out of the budget of our project. Submitted project proposal and worked on and submitted team contract.\ 

Had the intial conversation with the machine shop, to talk about our project ideas, said it was possible to fabricate the dispenser and gave us ideas how we could go about it got contact information.


## 2/11/2024 and 2/12/2024 
Objective is to have second meeting with TA, and discuss our sensors, and where we could get a chlorine sensor. \
Initial idea was to use a chlorine, pH, and temperature sensors as part of our sensor unit.\
Started reaching out to other departments to find sensors. \
Had our second weekly TA meeting. (2/12) \
Talked to Selva and suggested reaching out to Chemistry department storeroom, to see if we could get access to sensors.


## 2/19/2024
Objective was to reach out to different departments and professors inquiring about what water sensors they had. \
Also to have weekly meeting with TA. \
Started reaching out to a lot of people and sent lots of emails for sensors...\
List of people contacted for sensors:\
carter52@illinois.edu\
gregn@illinois.edu\
ece-stores@illinois.edu\
gentille@illinois.edu\
campusrec@illinois.edu\ 
barros@illinois.edu\
xmcai@illinois.edu\
mkonar@illinois.edu\
jimbest@illinois.edu    Runs the water council and research on water at UIUC\
sustainability@illinois.edu  (iSEE)\

https://cee.illinois.edu/academics/areas/water-resources-engineering-and-science\
Water Resources Engineering Department\

Weekly TA meeting (2/19), and discussed updates about sensors, and talking about design document. \

Started working on design document.

## 2/22/2024 and 2/23/2024
Objective was to finsih design document and redo project proposal. \
Finished and submitted design document. \
Redid and submitted proposal for proposal regrade.\
Got one resposne with a pH meter from Dr. Qi of Water Engineering department, but no one has access to a chlorine sensor.\

## 2/26/2024
Objective is to have weekly TA meeting and provide updates. \
Weekly TA meeting, and discussed grading, and other updates about sensors, from responses from different \
departments and professors.


## 2/27/2024
Objective is to figure out what to do about the chlorine sensor, and have a good design review. \
No one still had a chlorine sensor, was unsure if pH meter would work.\
Dr. Qi said pH meter he has is a WTW Handheld Meter, pH/Oxi 340i, but it is 10/15 years old. \
As it was a very expensive meter that could collect the pH value but would not be able to integrate it into our project.\
Had our design review at 11am with Professor Schuh, went decently.\
Decided chlorine sensor was too expensive and too far out of the budget of the project.\
Amazon. Amazon.Com: TDS Sensor, www.amazon.com/. Accessed 1 May 2024. \
Dr. Qi also provided some other suggestions linked below, based on lab work. \
[pH Meter Manual](https://www.manualslib.com/manual/1532124/Wtw-Ph-Oxi-340i.html?page=3#manual) \
[pH Meter](https://www.coleparmer.com/search?searchterm=wtw+ph+meter) \
[Chlorine Sensor](https://www.hach.com/search?keywords=chlorine+pillows&fn1=parameter&fv1=Chlorine&fn2=application&fv2=Potable+Water) \
[Chlorine Sensor Option](https://www.hach.com/search?keywords=chlorine+sensor&fn1=marketClass&fv1=Municipal) \
[Turbidity Option](https://www.hach.com/search?keywords=turbidity) \
[Chlorine Analyzer](https://www.hach.com/p-cl17sc-colorimetric-chlorine-analyzers/8572700)

## 3/3/2024 - 3/5/2024 (Before Spring Break)
Objective was to meet with TA and discuss and finalize new sensor. \
Weekly TA meeting (3/4), discussed the following updates. \
Talked with different professors and mentors to figure out alternative solutions to chlorine sensors.\
A mentor and professor suggested we could use a Total Dissolved Solvents (TDS) sensor, although it wouldn't\
give us the exact chlorine value we were looking for, it was an option to increase complexity and would\
provide useful data about chlorine and other chemicals.\ 
Decided to proceed with the TDS sensor.\
![](machine_shop_design.png)
Worked and finished the Sensor PCB to submit for first round of PCB orders so we could have it when we got\
back from spring break. \
DFRobot. "Gravity: Analog TDS Sensor/ Meter for Arduino." DFRobot, www.dfrobot.com/product-1662.html. \
![](sensor_pcb.png)

## 3/6/2024 - 3/8/2024 (Before Spring Break)
Objective is to finish team work evaluation and figure out machine shop design and place orders for parts. \
Wrote and submitted team work evaluation.\
Worked with machine shop and got design in so they could start building our product over spring break.\
Went back and forth several times with machine shop, working on the design of the dispensers. Found\ 
and ordered long cylinder containers for the machine shop to use for storage, and purchased the stepper\
motors and gave the machine shop all the parts before spring break.\


Placed 3 large orders of parts through our funds, so that all our components would arrive when we got back\
from Spring break.

## 3/18/2024 - 3/22/2024
Objective is to meet with TA and provide updates, and figure out PCB and parts pick up. \
Weekly TA meeting (3/18), right after spring break, talked about orders and picked up parts. \
We didn't submit anything for the second round of PCB orders, the day after we got back from spring break.\
Started on dispenser PCB and redesigned sensor PCB to get them ready for round 3.\
Worked with machine shop to get dispenser built, they had not started on our project yet. \
Recieved all amazon and other parts from orders.\
Had an issue with Digikey order where it would not be placed, so had to replace it, this set us back a few \
days, as it was unexpected delay, we thought all the orders had gone through smoothly. \
![](dispenser_pcb.png)

## 3/25/2024 - 3/26/2024
Objective is to meet with TA and provide updates and figure out building the dispenser with machine shop. \
Weekly TA meeting (3/25), and discussed the following subjects. \
Ran into a hiccup, the engineer we were working with in the machine shop got into an accident and was out\
for several days. So our project got passed on to another engineer and his already existing list of projects.\
Got in touch with the new engineer and started talking about our project and dispenser. \
Got the next round of PCB's audited and submitted for order.\
Wrote and submitted individual progress report. \
![](disp.png)


## 3/29/2024 - 4/2/2024
Objective was to provide updates to TA and redo design document. \
Weekly TA meeting (4/1), I missed this meeting as I had gone home for Easter and was unable to attend. \
My group kept my in touch and these are the updates we had. \
Redid design document by the regrade deadline to get as many points back as we could. \
Worked on PCBs and fixed what could be possible mistakes, started figuring out coding using DevKits, and \
began looking into alternative options because of PCB delays etc, and for testing purposes. 


```
/*********
  Rui Santos (https://RandomNerdTutorials.com/esp-now-one-to-many-esp32-esp8266/)
  
*********/

#include <esp_now.h>
#include <WiFi.h>
#include <Stepper.h>
#include <math.h>

// Motor Driver Pins
#define t1 8
#define t2 9
#define t3 10
#define t4 11

#define a1 12
#define a2 13
#define a3 14
#define a4 15

#define b1 8
#define b2 9
#define b3 10
#define b4 11

const int stepsPerRevolution = 100;

//Structure example to receive data
//Must match the sender structure
typedef struct test_struct {
  float ph;
  float tds;
  float temp;
} test_struct;

//Create a struct_message called myData
test_struct myData;
float tds;
float ph = 7;
float temp;
int tds_flag = 0;

//callback function that will be executed when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&myData, incomingData, sizeof(myData));
  Serial.print("Bytes received: ");
  Serial.println(len);
  Serial.print("ph: ");
  Serial.println(myData.ph);
  Serial.print("  TDS: ");
  Serial.println(myData.tds);
  Serial.print("  Temp: ");
  Serial.println(myData.temp);
  Serial.println();

  /// ASSIGN VARIABLES HERE
  ph = myData.ph;
  tds = myData.tds;
  temp = myData.temp;

}

// initialize the stepper library
Stepper chlor_stepper(stepsPerRevolution, t1, t2, t3, t4);
Stepper acid_stepper(stepsPerRevolution, a1, a2, a3, a4);
Stepper basic_stepper(stepsPerRevolution, b1, b2, b3, b4);
 
void setup() {
  //Initialize Serial Monitor
  Serial.begin(115200);
  
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  //stepper motors init
    chlor_stepper.setSpeed(150);
    acid_stepper.setSpeed(150);
    basic_stepper.setSpeed(150);
  
  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_register_recv_cb(OnDataRecv);
}
 
int loop_num;
void loop() {
  // tds = 11500;
  delay(4000);
  ph = 7.1;
  if (ph < 7.2) {
    loop_num = ceil((7.6 - ph)/0.1);          // one rotation for every 0.1 ph lower than normal --> change 0.1 to actual value
    // Serial.print(loop_num);
    chlor_stepper.step(stepsPerRevolution*9*loop_num);
    // Serial.print("  One round  ");
  }
  
  if (ph > 7.8) {
    loop_num = floor((7.4 - ph)/0.1);         // change 0.1 to how much increases
    acid_stepper.step(stepsPerRevolution);
  }

  // if (tds_flag == 1) {
  //   if (old_tds > tdsValue+25) {
  //     tds_flag = 0;
  //     Serial.println("Chlorine helped lower TDS value.");
  //   } else {
  //     Serial.println("Chlorine did not change TDS value. Please filter your water.");
  //     tds_flag = 0;
  //   }
  if (tds > 1000) {
    loop_num = (tds - 1000)/12;         // how much it actually lowers (normally 1/10000 lbs/gal for 12ppm increase)
    chlor_stepper.step(stepsPerRevolution);
  }

  //TDS output
  // Serial.print("  TDS Value:");
  // Serial.print(tds,0);
  // Serial.println("ppm");

  //pH output
  // Serial.print("  pH Value:");
  // Serial.print(ph);

  // if (temp < 26 || temp > 28) {
  //   Serial.print("Temperature is out of range.");
  // } else {
  //   Serial.print("Temperature is within the proper range.");
  // }

  // ph = 7.5;
  delay(4000); // one minute

}
```


## 4/8/2024
Objective is to meet with TA and figure out mock demo and discuss updates about machine shop. \
Met with Selva during weekly meeting (4/8) and told him about the issues and delays so agreed to move mock demo \ 
to Thursday from Monday. Discussed the accident and the problems we had in the machine shop side. And \
discussed the PCB delays and how that pushed our timeline back.

## 4/13/2024 - 4/16/2024
Objective was to put everything together and test dispenser. \
Got a dispenser built from the machine shop, and solders PCB's to begin testing, and tested the code. \
When testing dispenser, the wheels would not spit, but could tell the motor was trying. \
We tried to fix it on the software side, but could not get it to work.\
As our goal was the dispenser unit will release 0.018 cubic inches of chemicals with a 5% standard \
deviation (0.0171in3 to 0.0189in3) using stepper motors according to microcontroller instructions. \
The motor must be able to spin 360 degrees ± 5 degrees to maintain consistent performance for repeated \ 
actions. 0.018 cubic inches of chemicals will be dispensed every rotation, with 5% standard \ 
deviation 0.018±0.0009in3. \
![](dispenser.png)


## 4/18/2024
Objective is to have a mock demo and figure out how the actual demo would go. \
Weekly TA meeting (4/18) and this was our mock demo. We discussed worst case scenario of our scoring based on \
all the materials and project we had at the time. \
We had four days to make as much progress as possible, as our demo was schedule for 4/23 at 4pm. \
Selva gave us some tips on what we could try and do, abondoned PCBs and decided to move to breadboards and use \
the devkits. Ordered some parts during our meeting, and hoping for very quick delivery.

## 4/19/2024
Objective is to figure out what was wrong with dispenser. \
Went back and forth multiple times with machine shop, to adjust the wheel. Trimmed off a little bit each time, then \
tried to loosen the screws holding it all together. Tried many different things was unable to get it dispensing.

## 4/20/2024
My objective was to figure out how to make the dispenser work. \
I came back and took apart the dispenser,and found loosened a lot of the parts and was finally able to get the wheel \
turning. I found out where it was getting stuck and tried to create a temporary fix. I was able to get the wheel \
spinning eventually. But I needed to take it to the machine shop on Monday morning to get it fixed in those areas. \
Began testing the coding to see if it would work, and what would happen. Began testing the sensor suite as well, to \
see if we could get readings and based on adjusting the chemical levels of the water and would happen. We set up \
alerts and got a lot of the software side working. \
Goal for the sensor suite was the pool sensors must accurately measure and output analog readings for the water \
quality, with temperature between 78-82 degrees, pH between 7.2 to 7.8, and total dissolved solids level (TDS) levels \ between 0-1000 ppm. The standard deviation for temperature should be within 1 degree, pH should be within 0.1, and TDS \ levels should be within 100 ppm. \

```
// source: https://wiki.keyestudio.com/KS0429_keyestudio_TDS_Meter_V1.0#Test_Code
// Project: https://RandomNerdTutorials.com/esp32-tds-water-quality-sensor/

#define TdsSensorPin 4
#define VREF 3.3              // analog reference voltage(Volt) of the ADC
#define SCOUNT  50            // sum of sample point

#define pH_sensor 5
float phValue = 0;

#define temp_sensor 6

int tds_buf[SCOUNT];     // store the analog value in the array, read from ADC
int tds_buf_temp[SCOUNT];
int tds_buf_idx = 0;
int copyIndex = 0;

int temp_buf[SCOUNT];     // store the analog value in the array, read from ADC
int temp_buf_temp[SCOUNT];
int temp_buf_idx = 0;

int ph_buf[SCOUNT];     // store the analog value in the array, read from ADC
int ph_buf_temp[SCOUNT];
int ph_buf_idx = 0;

unsigned long int avgValue;  //Store the average value of the sensor feedback
float b;
int buf[10],temp;

float avg_voltage_tds = 0;
float avg_voltage_ph = 0;
float avg_voltage_temp = 0;
float tdsValue = 0;
float temperature = 25;       // current temperature for compensation

float input_voltage;
float temp_val;
int pullup_resistor;
float temp_pot;
float real_temp;



// median filtering algorithm
int getMedianNum(int bArray[], int iFilterLen){
  int bTab[iFilterLen];
  for (byte i = 0; i<iFilterLen; i++)
  bTab[i] = bArray[i];
  int i, j, bTemp;
  for (j = 0; j < iFilterLen - 1; j++) {
    for (i = 0; i < iFilterLen - j - 1; i++) {
      if (bTab[i] > bTab[i + 1]) {
        bTemp = bTab[i];
        bTab[i] = bTab[i + 1];
        bTab[i + 1] = bTemp;
      }
    }
  }
  if ((iFilterLen & 1) > 0){
    bTemp = bTab[(iFilterLen - 1) / 2];
  }
  else {
    bTemp = (bTab[iFilterLen / 2] + bTab[iFilterLen / 2 - 1]) / 2;
  }
  return bTemp;
}

void setup(){
  Serial.begin(115200);
  pinMode(TdsSensorPin,INPUT);
  pinMode(pH_sensor,INPUT);
  // pinMode(temp_sensor,INPUT);
}

void loop(){
  static unsigned long analogSampleTimepoint = millis();
  if(millis()-analogSampleTimepoint > 40U){     //every 40 milliseconds,read the analog value from the ADC
    analogSampleTimepoint = millis();
    tds_buf[tds_buf_idx] = analogRead(TdsSensorPin);    //read the analog value and store into the buffer
    tds_buf_idx++;
    if(tds_buf_idx == SCOUNT){ 
      tds_buf_idx = 0;
    }

    // ph_buf[tds_buf_idx] = analogRead(pH_Sensor);    //read the analog value and store into the buffer
    // ph_buf_idx++;
    // if(ph_buf_idx == SCOUNT){ 
    //   ph_buf_idx = 0;
    // }

    // temp_buf[ph_buf_idx] = analogRead(temp_sensor);    //read the analog value and store into the buffer
    // temp_buf_idx++;
    // if(temp_buf_idx == SCOUNT){ 
    //   temp_buf_idx = 0;
    // }
  }
  
  static unsigned long printTimepoint = millis();
  if(millis()-printTimepoint > 800U){
    printTimepoint = millis();
    for(copyIndex=0; copyIndex<SCOUNT; copyIndex++){
      tds_buf_temp[copyIndex] = tds_buf[copyIndex];
      
      // read the analog value more stable by the median filtering algorithm, and convert to voltage value
      avg_voltage_tds = getMedianNum(tds_buf_temp,SCOUNT) * (float)VREF / 4096.0;
      // avg_voltage_ph = getMedianNum(ph_buf_temp,SCOUNT) * (float)VREF / 4096.0;
      // avg_voltage_temp = getMedianNum(temp_buf_temp,SCOUNT) * (float)(4.5) / 4096.0;
      
      //temperature compensation formula: fFinalResult(25^C) = fFinalResult(current)/(1.0+0.02*(fTP-25.0)); 
      float compensationCoefficient = 1.0+0.02*(temperature-25.0);
      //temperature compensation
      float compensationVoltage=avg_voltage_tds/compensationCoefficient;
      
      //convert voltage value to tds value
      tdsValue=(133.42*compensationVoltage*compensationVoltage*compensationVoltage - 255.86*compensationVoltage*compensationVoltage + 857.39*compensationVoltage)*0.5;

      // input_voltage = 4.5;
      // temp_val = avg_voltage_temp;
      // pullup_resistor = 5110;
      // temp_pot = (temp_val * pullup_resistor) / (input_voltage - temp_val);
      // real_temp = (temp_pot - 1851) / 7.5;
    }

    for(int i=0;i<10;i++)       //Get 10 sample value from the sensor for smooth the value
  { 
    buf[i]=analogRead(pH_sensor);
    delay(10);
  }
  for(int i=0;i<30;i++)        //sort the analog from small to large
  {
    for(int j=i+1;j<10;j++)
    {
      if(buf[i]>buf[j])
      {
        temp=buf[i];
        buf[i]=buf[j];
        buf[j]=temp;
      }
    }
  }
  }

  // pH value calculation
  // for(int i=0;i<10;i++)       //Get 10 sample value from the sensor for smooth the value
  // { 
  //   buf_ph[i]=analogRead(pH_Sensor);
  //   delay(10);
  // }
  // for(int i=0;i<9;i++)        //sort the analog from small to large
  // {
  //   for(int j=i+1;j<10;j++)
  //   {
  //     if(buf_ph[i]>buf_ph[j])
  //     {
  //       temp=buf_ph[i];
  //       buf_ph[i]=buf_ph[j];
  //       buf_ph[j]=temp;
  //     }
  //   }
  // }
  // avgValue=0;

  // for(int i=2;i<8;i++)                      //take the average value of 6 center sample
  //   avgValue+=buf_ph[i];
  // float phValue=(float)avgValue*5.0/1024/6; //convert the analog into millivolt
  // phValue=3.5*phValue;

  // input_voltage = 4.5;
  // avg_voltage_temp = getMedianNum(temp_buf_temp,SCOUNT) * (float)(4.5) / 4096.0;
  // temp_val = avg_voltage_temp;
  // pullup_resistor = 5110;
  // temp_pot = (temp_val * pullup_resistor) / (input_voltage - temp_val);
  // real_temp = (temp_pot - 1851) / 7.5;

  // Serial.print(" analog read:");
  // Serial.print(temp_val);
  // Serial.print("  temp pot ");
  // Serial.print(temp_pot, 0);
  // Serial.print("  real temp ");
  // Serial.print(real_temp, 0);
  // Serial.println("C");

  // float Value= analogRead(pH_sensor);
  // // Serial.print(Value);
  // // Serial.print(" | ");
  // float voltage=Value*(3.3/4095.0);
  // phValue=(3.3*voltage);
  // // Serial.println(phValue);
  // delay(500);

  
  avgValue=0;
  for(int i=2;i<8;i++)                      //take the average value of 6 center sample
    avgValue+=buf[i];
  float phValue=(float)(avgValue/6)*5.0/1024/6; //convert the analog into millivolt
  phValue=3.5*phValue;

  //TDS output
  Serial.print("  TDS Value:");
  Serial.print(tdsValue,0);
  Serial.println("ppm");

  // //pH output
  Serial.print("  pH Value:");
  Serial.print(phValue);


  delay(500);

}

/*********
  Rui Santos https://RandomNerdTutorials.com/esp-now-one-to-many-esp32-esp8266/
  
*********/

#include <esp_now.h>
#include <WiFi.h>

// REPLACE WITH YOUR ESP RECEIVER'S MAC ADDRESS
uint8_t broadcastAddress1[] = {0x68, 0xB6, 0xB3, 0x52, 0x81, 0x1C};

typedef struct test_struct {
  float ph;
  float tds;
  float temp;
} test_struct;

test_struct test;

esp_now_peer_info_t peerInfo;

// callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  char macStr[18];
  Serial.print("Packet to: ");
  // Copies the sender mac address to a string
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  Serial.print(macStr);
  Serial.print(" send status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}
 
void setup() {
  Serial.begin(115200);
 
  WiFi.mode(WIFI_STA);
 
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  
  esp_now_register_send_cb(OnDataSent);
   
  // register peer
  peerInfo.channel = 0;  
  peerInfo.encrypt = false;
  // register first peer  
  memcpy(peerInfo.peer_addr, broadcastAddress1, 6);
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }
}
 
void loop() {
  test.ph = 7.0;
  test.tds = 1000;
  test.temp = 25;
 
  esp_err_t result = esp_now_send(0, (uint8_t *) &test, sizeof(test_struct));
   
  if (result == ESP_OK) {
    Serial.println("Sent with success");
  }
  else {
    Serial.println("Error sending the data");
  }
  delay(2000);
}

```

## 4/22/2024
My object was to get the dispenser working. \
Went to the machine shop back and forth several times, and was able to get the wheel spinning to some degree. However \
it was unable to dispense anything, since as soon as the powder was added, the powder would get stuck throughout the \
wheel and it would stop the dispensing mechanism. So powder was a no go, so tried to find small pellets and to make the \
hole in wheel bigger, but was unable to find the appropriate size chemicals in time. \
Also worked with the machine shop to come up with an enclosure for the dispenser and an enclosure for the sensor suite, \
to protect and hide all the wires. We were able to fianlly put it all together and were able to get the dispenser spinning \
based on values we got from the sensor. \
![](sensor_suite.png) \
![](sensors.png)


## 4/23/2024
Object was to demo the project and our high level requriements. \
Demo at 4pm with Professor Schuh. Went decently, was able to show full functionality of all but one of our high \
level requirements. 

## 4/24/2024 - 4/26/2024
Objective was to prepare for the mock presentation \
Made presentation slides, and started figuring out what to present and what we needed for presentation. \
[Presentation Slides](https://gitlab.engr.illinois.edu/rc18/ece445-notebook/-/blob/main/notebooks/arnold/UIUC-ECE445_Final__2_.pdf?ref_type=heads)

## 4/26/2024
Objective was to do well on the mock presentation and get good feedback from the communication team. \
Mock presentation with communication team. Went decently, got some good feedback about the presentation and how \
to present. Took in the comments and modified the slides, and added more content. Presentation was under 10 minutes.
Made EC video showing our project and its functionality.

## 4/27/2024 - 4/29/2024
Object was to make presentation longer and more thorough and prepare for final presentation. \
Worked on adding content to our presentaiton and adding the parts the communication teams suggested. Made presentation \
longer to get it closer to the Objective, and added more slides.

## 4/29/2024
Objective was to do well on the final presentation. \
Final presentation at 4:30pm. Presented in front of Professor Schuh, TAs, and Peer Reviewers. Was asked one question, \
which we were able to answer.

## 4/30/2024 - 5/1/2024
Objective was to work on, finish, and submit the final paper. \
Worked on final paper, and submitted on 5/1. Then prepared for lab checkout tomorrow. \
[Final Paper](https://gitlab.engr.illinois.edu/rc18/ece445-notebook/-/blob/main/notebooks/arnold/ECE445_Final_Report.pdf?ref_type=heads)