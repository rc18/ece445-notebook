# ECE 445 - Senior Design Lab Notebook

## Week 2/6 - 2/17

Found our idea: automated pool quality regulator
- looked for sensors we need for the project
- started asking other departments for sensors


## Week 2/18:

Goals for the week:
- collecting parts:
  - ph sensor, chlorine, temperature sensor (still waiting to hear from these)
  - motors/servos for dispenser 
  - plastic jars
  - powders for testing (should probably just get them now)
  - figure out which microcontroller
- finalize details for design document

Notes:
- chlorine sensor is really expensive, but might lose complexity points if we don't have this
- send emails to ask about if any departments have these 


## Week 2/25:

Goals for the week:
- Get all parts list (including all circuitry)
- Try to send out a rough draft of PCB designs through audit and order
- Figure out how to do motor PCB
- Talk to machine shop about dispenser idea!!

Did rough draft of PCB design for dispenser, but did not get it into the audit this week:

![](pcb_1.PNG)


## Week 3/3:

Goals for the week:
- SUBMIT dispenser PCB design!
- start looking at how to code for the project
- finalize dispenser plans with machine shop this week!! (high priority)
  - decide on design choice
  - bring in parts by Friday 4pm!

Notes this week:
- chlorine sensor is really expensive, so a TDS might be a way to detect chlorine levels instead.
- Sensor PCB was sent in for audit
- Dispenser design: using 5V stepper motors + 3 plastic jars for chemicals
- ordered parts (should be here after spring break)!


## Week 3/10

Spring Break - no new progress this week


## Week 3/17

Goals for the week:
- Dispenser pcb MUST be sent out this week!
- revisions for sensor pcb
- start coding something!

Notes:
- Dispenser PCB was not sent out, HAS to be next week (highest priority)
- Issue with placing one of the orders before spring break, replaced it this week :(
- submitted our designs to the machine shop and they will start working on it soon
- got some coding resources together:
  - Motor Code:
    https://randomnerdtutorials.com/esp32-stepper-motor-28byj-48-uln2003/
  - pH Meter code:
    https://how2electronics.com/diy-iot-water-ph-meter-using-ph-sensor-esp32/#:~:text=To%20power%20the%20pH%20Sensor,with%20analog%20pins%20of%20ESP32.


## Week 3/24

Goals this week:
- Work on coding, at least figure out some of the code (sensors first)
- SUBMIT PCB (high priority) !!
- Order more parts if necessary

Notes:
- got the pH and TDS sensors to work separately, now need to put the code together and work on making it more accurate. The readings are very sporadic right now. Our microcontroller is not compatible with Arduino IDE, so tried to download and figure out how to use ESP-IDF instead. This is going very poorly, might have to switch our microcontroller to make it feasible to code the microcontroller. 
  - TDS Sensor code: https://randomnerdtutorials.com/esp32-tds-water-quality-sensor/
https://how2electronics.com/ph-meter-using-ph-sensor-arduino-oled/
- unfortunetly, someone at machine shop got injured. Someone else was given our design and they will work on it. Might be some delay with it, but not a huge issue.
- Submitted our dispenser PCB!


## Week 3/31

Goals for the week:
- finish coding on Arduino IDE first, then resolve issues. At least get all of the code written somewhere.
- Revise PCBs for one of the last rounds 

Notes:
- we switched our PCB design to use another microcontroller because we were having issues with our ESP32-c6. We switched to the ESP32-s3. This makes it much easier to code (Arduino IDE instead of ESP-IDF). Yay!
- The code for the pH and TDS is written. Found code for the stepper motors which can be changed for our project easily. Looked into communicating between two microcontrollers and decided to use espNow which uses a WiFi/p2p protocol.
  - espNow: https://randomnerdtutorials.com/esp-now-one-to-many-esp32-esp8266/
- Dispenser board updated PCB:

![](pcb_2.PNG)


## Week 4/7

Goals for the week (homestretch):
- Make ANY last-minute pcb updates and send them in!
- Finish coding (high prioroty)
- communicate with machine shop about updates on our dispenser (mock demo is next week we need it!)

Notes:
- PCBs were delayed so we did not order another round. We just have to hope the last round's pcb worked
- We still don't have our dispenser, time estimate is early next week (mock demo is next thurs)
- Code for motors and sensors are complete, but need to test it. Waiting on the dispenser from machine shop to test it with those motors. Looking into the code for espNow protocol. Need to follow a few instructions still to implement this.
  - espNow: https://www.youtube.com/watch?v=CbcnxGJkY-Q&ab_channel=BaldGuyDIY
  - change MAC addy: https://randomnerdtutorials.com/get-change-esp32-esp8266-mac-address-arduino/


## Mock Demo Week 4/14-4/18

Goals this week:
- literally everything
- get the project working with only minimal bugs (at least) BY THURS!!
- we got our pcbs and need to solder them and test them
- we need to get our design from the machine shop and test it ASAP

Notes:
- the motors on our dispenser do not seem to work. We bought another motor and that motor was working, so we need to fix the wheels that the motor is supposed to move in some way (they gotta move!).
- our pcb designs do not seem like they are working. During our meeting with Selva, he gave us a trick we can use to try to make the PCB work since it seems to be a USB-UART connection  problem that is preventing us from getting the code onto the microcontroller.
- we are considering moving to a breadboard using devKits instead since the PCBs did not work.
- we need one more DevKit! The lab is out of esp32-s3 DevKits. I ordered some on amazon that are supposed to come on Saturday (hopefully they do). 
- Need to fix code and add espNow protocol. It cannot be tested until Saturday until the other microcontroller comes in, though.



Working: our sensors work individually. The motor NOT attached to our dispenser design is working with the code. 

Still needs to work: forgot about temperature sensor!! Also need to get motors moving and check if they work simultaneously. Need to test espNow protocol.


## Final Demo Week 4/19-4/23

Goals for the week:
- EVERYTHING 
- get all parts on the breadboard.
- fix motor problem on our dispenser
- get temperature sensor working 
- get espNow working

Notes:
- The motors did move after the wheel was loosened, however, that made the powders get stuck in the gap and the motors could not push through and got stuck. Therefore, we will demo with no powder for the functionality of it all working.
- the other microcontroller came on Saturday. yay! Therefore, we had three days to test everything together and get it to work.
- The temperature sensor works on resistance and there are limited resources online. Therefore, based on the datasheet we firgured out the equation. This gave us the most trouble the last few days, but it did work correctly by demo day.
- the connection between the microcontrollers (espNow) worked well.
- All the parts finally work together, except for being unable to get actual powders to work with the motors.
- we tried to make the powders into little pellets or balls so they would not get stuck between the gaps of the wood and wheel, however, we could not get it to happen in a functional way. Therefore, we will demo without powders to show proof of concept and overall functionality of our project.
- we got a few tweaks on our dispenser a day before our demo. We also got an enclosure for the dispenser unit and sensor unit to protect all of our components.
- IT WORKED THOUGH YAYY


## Week 4/23-4/30

We are done with our project/product component, we are just working on our final presentation and final paper now! YAY!!




