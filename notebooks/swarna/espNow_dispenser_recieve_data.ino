/*********
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp-now-one-to-many-esp32-esp8266/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*********/

#include <esp_now.h>
#include <WiFi.h>
#include <Stepper.h>
#include <math.h>

// Motor Driver Pins
#define t1 8
#define t2 9
#define t3 10
#define t4 11

#define a1 12
#define a2 13
#define a3 14
#define a4 15

#define b1 8
#define b2 9
#define b3 10
#define b4 11

const int stepsPerRevolution = 100;

//Structure example to receive data
//Must match the sender structure
typedef struct test_struct {
  float ph;
  float tds;
  float temp;
} test_struct;

//Create a struct_message called myData
test_struct myData;
float tds;
float ph = 7;
float temp;
int tds_flag = 0;

//callback function that will be executed when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&myData, incomingData, sizeof(myData));
  Serial.print("Bytes received: ");
  Serial.println(len);
  Serial.print("ph: ");
  Serial.println(myData.ph);
  Serial.print("  TDS: ");
  Serial.println(myData.tds);
  Serial.print("  Temp: ");
  Serial.println(myData.temp);
  Serial.println();

  /// ASSIGN VARIABLES HERE
  ph = myData.ph;
  tds = myData.tds;
  temp = myData.temp;

}

// initialize the stepper library
Stepper chlor_stepper(stepsPerRevolution, t1, t2, t3, t4);
Stepper acid_stepper(stepsPerRevolution, a1, a2, a3, a4);
Stepper basic_stepper(stepsPerRevolution, b1, b2, b3, b4);
 
void setup() {
  //Initialize Serial Monitor
  Serial.begin(115200);
  
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  //stepper motors init
    chlor_stepper.setSpeed(150);
    acid_stepper.setSpeed(150);
    basic_stepper.setSpeed(150);
  
  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_register_recv_cb(OnDataRecv);
}
 
int loop_num;
void loop() {
  // tds = 11500;
  delay(4000);
  ph = 7.1;
  if (ph < 7.2) {
    loop_num = ceil((7.6 - ph)/0.1);          // one rotation for every 0.1 ph lower than normal --> change 0.1 to actual value
    // Serial.print(loop_num);
    chlor_stepper.step(stepsPerRevolution*9*loop_num);
    // Serial.print("  One round  ");
  }
  
  if (ph > 7.8) {
    loop_num = floor((7.4 - ph)/0.1);         // change 0.1 to how much increases
    acid_stepper.step(stepsPerRevolution);
  }

  // if (tds_flag == 1) {
  //   if (old_tds > tdsValue+25) {
  //     tds_flag = 0;
  //     Serial.println("Chlorine helped lower TDS value.");
  //   } else {
  //     Serial.println("Chlorine did not change TDS value. Please filter your water.");
  //     tds_flag = 0;
  //   }
  if (tds > 1000) {
    loop_num = (tds - 1000)/12;         // how much it actually lowers (normally 1/10000 lbs/gal for 12ppm increase)
    chlor_stepper.step(stepsPerRevolution);
  }

  //TDS output
  // Serial.print("  TDS Value:");
  // Serial.print(tds,0);
  // Serial.println("ppm");

  //pH output
  // Serial.print("  pH Value:");
  // Serial.print(ph);

  // if (temp < 26 || temp > 28) {
  //   Serial.print("Temperature is out of range.");
  // } else {
  //   Serial.print("Temperature is within the proper range.");
  // }

  // ph = 7.5;
  delay(4000); // one minute

}