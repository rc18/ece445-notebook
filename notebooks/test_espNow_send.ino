/*********
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp-now-one-to-many-esp32-esp8266/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*********/

#include <esp_now.h>
#include <WiFi.h>

#define TdsSensorPin 4
#define VREF 3.3              // analog reference voltage(Volt) of the ADC
#define SCOUNT  50            // sum of sample point

#define pH_Sensor 5
float phValue = 0;

#define temp_sensor 6

int tds_buf[SCOUNT];     // store the analog value in the array, read from ADC
int tds_buf_temp[SCOUNT];
int tds_buf_idx = 0;
int copyIndex = 0;

float avg_voltage_tds = 0;
float tdsValue = 0;
float temperature = 25;       // current temperature for compensation

unsigned long int avgValue;  //Store the average value of the sensor feedback
float b;
int buf[10],temp;

// REPLACE WITH YOUR ESP RECEIVER'S MAC ADDRESS
uint8_t broadcastAddress1[] = {0x68, 0xB6, 0xB3, 0x52, 0x81, 0x1C};

typedef struct test_struct {
  float ph;
  float tds;
  float temp;
} test_struct;

test_struct test;

esp_now_peer_info_t peerInfo;

// callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  char macStr[18];
  Serial.print("Packet to: ");
  // Copies the sender mac address to a string
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  Serial.print(macStr);
  Serial.print(" send status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
  Serial.println("");
}
 
void setup() {
  Serial.begin(115200);
  pinMode(TdsSensorPin,INPUT);
  pinMode(pH_Sensor,INPUT);
  pinMode(temp_sensor, INPUT);

  WiFi.mode(WIFI_STA);
 
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  
  esp_now_register_send_cb(OnDataSent);
   
  // register peer
  peerInfo.channel = 0;  
  peerInfo.encrypt = false;
  // register first peer  
  memcpy(peerInfo.peer_addr, broadcastAddress1, 6);
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }
}
 

unsigned long loop_delay;
float temp_sum;
int temp_index = 1;
float average_temp, temp_val, temp_pot;
float real_temp, temp_cal;
int temp_flag = 1;
float average_tds, tds_analog, real_tds, tds_sum, old_tds;
int tds_index = 1;
int tds_flag = 0;
static float delay_length = 30000;

void loop() {

  delay(10);
  temp_index = 1;
  tds_index = 1;
  tds_sum = 0;
  loop_delay = millis();
  temp_sum = 0;

  while(tds_index < 150){
    tds_analog = analogRead(TdsSensorPin) * 3.3 / 4096;
    tds_sum += tds_analog;
    tds_index++;
  }
  average_tds = tds_sum / tds_index;
  float compensationCoefficient = 1.0+0.02*(temperature-25.0);
  //temperature compensation
  float compensationVoltage=average_tds/compensationCoefficient;
  //convert voltage value to tds value
  tdsValue=(133.42*compensationVoltage*compensationVoltage*compensationVoltage - 255.86*compensationVoltage*compensationVoltage + 857.39*compensationVoltage)*0.5;
  Serial.print("TDS Value:");
  Serial.print(tdsValue,0);
  Serial.println("ppm");


  delay(10);
  // pH value calculation
  for(int i=0;i<10;i++)       //Get 10 sample value from the sensor for smooth the value
  { 
    buf[i]=analogRead(pH_Sensor);
    delay(10);
  }
  for(int i=0;i<9;i++)        //sort the analog from small to large
  {
    for(int j=i+1;j<10;j++)
    {
      if(buf[i]>buf[j])
      {
        temp=buf[i];
        buf[i]=buf[j];
        buf[j]=temp;
      }
    }
  }
  avgValue=0;

  for(int i=2;i<8;i++)                      //take the average value of 6 center sample
    avgValue+=buf[i];
  float phValue=(float)avgValue*3.3/4095/6; //convert the analog into millivolt
  phValue=3.5*phValue;


  delay(10);
  // Temp code
    while(temp_index < 150){
      temp_val = analogRead(temp_sensor) * 4.5 / 4096;
      // Serial.println(temp_val);
      temp_sum += temp_val;
      temp_index++;
    }
  
  average_temp = temp_sum / temp_index;
  if(temp_flag ==1){
    temp_pot = (average_temp * 5110) / (4.5 - average_temp);
    temp_cal = temp_pot - 2025;
    temp_flag = 0;
  }
  temp_pot = (average_temp * 5110) / (4.5 - average_temp) - temp_cal;
  real_temp = (temp_pot - 1718) / 4.167;


  Serial.print("Temperature (F): ");
  Serial.println(real_temp);

  //TDS output


  //pH output
  Serial.print("pH Value:");
  Serial.println(phValue);


  // tdsValue = 1100;
  test.ph = phValue;
  test.tds = tdsValue;
  test.temp = real_temp;
 
  esp_err_t result = esp_now_send(0, (uint8_t *) &test, sizeof(test_struct));
   
  if (result == ESP_OK) {
    Serial.println("Sent with success");
  }
  else {
    Serial.println("Error sending the data");
  }




  if (phValue < 7.2) {
    Serial.println("pH Low, dispensing basic chemical");
  }
  
  if (phValue > 7.8) {
    Serial.println("pH High, dispensing acidic chemical");
  }

  if (tds_flag == 1) {
    if (old_tds > tdsValue+25) {
      tds_flag = 0;
      Serial.println("Chlorine helped lower TDS value.");
    } else {
      Serial.println("Chlorine did not change TDS value. Please filter your water.");
      tds_flag = 0;
    }
  } else if (tdsValue > 1000) {
    Serial.println("TDS High, dispensing chlorine.");

    old_tds = tdsValue;
    tds_flag = 1;
  } else {
    tds_flag = 0;
  }

  if (72 < real_temp && real_temp < 76) {
    Serial.println("Temperature is within the correct range.");
  } else {
    Serial.println("Temperature is not within the correct range.");
  }
  while(millis() - loop_delay < delay_length){

  }
}