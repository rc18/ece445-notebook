# Raymond's Notebook


# 2/18/2024
Collected parts for our project, such as sensors and circuitry components. Put in preliminary orders for parts from digikey and Amazon.

# 3/5/2024
Designed first round of sensor PCB. This iteration does not have the LED displays yet, only connections for battery and sensors so we can test and do calculations as well as experiment with wireless transmission. Submitted this PCB for the first audit round of PCB deliveries.

![](Sensor_board_v1_schematic.png)

![](Sensor_board_v1_PCB.png)


# 3/25/2024
Helped submit a version of our dispenser PCB for 3rd roundd of audit, however within the same week found an error in design, so we have to submit another round for the next week. 

#4/1/2024

Updated Sensor PCB to have the new microncontroller (ESP32-S3) which is more compatable with Arduino IDE and libraries. Improved sensor input schematic to match requirements from each sensor.
![](Sensor_Board_v2_PCB.png)

Helped design the dispenser PCB board as well. Boot circuit is similar to sensor board's circuit, the main difference is the H-bridges for motor control. 

![](Dispenser_board_schematic.png)

![](Dispenser_board_PCB.png)

# 4/15/2024 Mock Demo week
Microcontroller on the PCBs could not connect to the computer through the usb-uart circuits, so we have to do a quick transition to breadboard design. Bought some ESP32 DevKits to continue working on our project. This week we built the breadboard circuitry and fixed any errors in our dispenser. Established wireless communication between the two microcontrollers so that the sensor and dispenser systems were separate and wireless. Dispenser motors were able to spin according to sensor readings.

# 4/22/2024 
Finished demoing our project for our final demonstration.