# ECE445-Notebook

## Group
Arnold Ancheril (arnolda2@illinois.edu) \
Raymond Chen (rc18@illinois.edu) \
Swarna Jammalamadaka (sjamma2@illinois.edu)

## Final Paper
[Final Paper](https://gitlab.engr.illinois.edu/rc18/ece445-notebook/-/blob/main/notebooks/arnold/ECE445_Final_Report.pdf?ref_type=heads)

## Final Presentation
[Presentation Slides](https://gitlab.engr.illinois.edu/rc18/ece445-notebook/-/blob/main/notebooks/arnold/UIUC-ECE445_Final__2_.pdf?ref_type=heads)

